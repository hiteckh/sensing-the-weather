# Sensing the Weather - Wind Direction Worksheet

In this lesson you will:

- Understand how the wind vane uses reed switches to change its output voltage
- Understand the difference between an analogue and a digital signal
- Be able to write a program to output the wind direction based on input from the wind vane

## How does the wind vane work?

A wind vane shows the direction *from which* the wind is coming, not where it's going; this can be confusing because TV weather maps show the opposite. It works by the wind exerting force on a vertical blade which rotates to find the position of least resistance; this position is then aligned with the direction of the oncoming wind.

The wind vane is more complex than the [rain gauge](../guides/rain_gauge.md) or [anemometer](../guides/wind_speed.md). It does use reed switches and magnets, but it works in a completely different way. 

If you look inside the wind vane, you'll see there are eight reed switches arranged like the spokes of a wheel.

![](images/wind_vane_reed.png)

So what's going on here? Firstly, we need to understand what a [resistor](http://en.wikipedia.org/wiki/Resistor) is. These are small components that resist/reduce the flow of electrical current but don't stop it; at the same time they also reduce the voltage moving through the circuit. Resistors can have different values; a low resistance value would let almost all voltage/current through, but a high resistance value would let very little through. There are eight resistors in the wind vane, and as the magnet rotates, different reed switches will open and close and thus switch their corresponding resistor in and out of the circuit.

Each of the eight resistors have different values which you'll see printed in white text next to them (e.g. you can see 8.2K on the right). This allows the wind vane to have 16 possible combinations of resistance, since the magnet is able to close two reed switches when halfway between them. More info can be found in the [datasheet](https://www.argentdata.com/files/80422_datasheet.pdf).

## Analogue vs. digital

We record a voltage from the wind vane which varies according to which combination of resistors is currently being switched into the circuit. This is an *analogue* signal because it continuously reports a **range** of voltages. When we used the anemometer it simply reported a `HIGH` or `LOW` voltage, all or nothing, which is a *digital* signal. To be able to interpret an analogue signal, we need a special component called an Analogue to Digital Convertor (ADC) to allow us to observe this signal as a number; the signal is *analogue* and our Raspberry Pi is a *digital* machine. The board inside your Weather Station has an ADC built in.

## Getting started

Start the BlueJ and create a new project called "Wind_Direction", within the project, create a new Java class called "WindVane"


## Using the wind_direction module

Reading information from the ADC is a little tricky, so we're going to use 
WindDirection Class which contains code to help in this exercise.

1. Let's import the follwoing packages:

	```java
	import org.bluej.WeatherStation.Units.WindDirection;
    import org.bluej.WeatherStation.Implementations.Factory;
    import org.bluej.WeatherStation.Implementations.Platform;
    import org.bluej.WeatherStation.Sensors.WindVaneSensor;
	```
1. In the Constructor, create a sensor object as an instance of the WindVanSensor.
The Factory class is a class for building the sensor set for a given target:


```java
public WindVane()
    {
        sensor = new Factory(Platform.WEATHERSTATION_V1).getWindVaneSensor();
    }
```
Note: If you are doing this exercise away from a working weather station,
you can change Platform.WEATHERSTATION_V1 to Platform.MOCK to receive fake data from the sensor instead.

1. Now we need to declare constants of the number of reading of the wind directions, and the interval time between each reading:
 
```java
    /**
     * How often to check the wind direction.
     */
    public static final int INTERVAL_MILLIS = 10000;

    /**
     * How many times to check the wind van when asked for a reading.
     */
    public static final int ITERATIONS = 4;
```
  
1. We also need to declare a sensor variable
 
```java
     private WindVaneSensor sensor;
```     
1. Then, create a new method called getReadings to read an array of the wind directions values from the wind vane:

```java
public WindDirection[] getReadings() throws InterruptedException {

        //read wind direction values within the time intervals and print the result on the termianl window

        return windDirections;
    }
``` 
 
1. We would like to sample the wind direction at a 10-second interval. There's a function inside so we can use 

Thread.sleep(INTERVAL_MILLIS);

2. To print the wind directions in degrees, there is method called getDegrees() of the class WindDirection 

You can test this by compiling, creating a new WindVane object, and calling getReadings.


## Which direction is the wind coming from?

### Question

Earlier, we talked about how the 8 reed switches can provide 16 possible values depending on the position of the vane. So how is it possible for the `getDegrees()` method to give us a reading in degrees, when there are 360 degrees in a circle and only 16 possible combinations of resistance?

### Answer

During the sampling interval, multiple readings are taken and we are given an *average* of those readings, converted into degrees.


### Question

How can we use the data from the wind vane (given in degrees) to show which compass direction the wind is coming from?

### Answer

The Met Office weather reports use a number of [wind symbols](http://www.metoffice.gov.uk/guide/weather/symbols#windsymbols) to show information about the wind, as well as giving a compass direction that the wind is blowing *from*.

Looking at this diagram with compass points, if we consider North to be 0 degrees then any of the compass points can be worked out as a bearing from North. The first has been shown for you as an example:

![Wind vane with some angles](images/wind_vane_degrees.png)

The WindDirection class has a method called getCompassDirection() which takes the value we received from the sensor, and instead of presenting the data as degrees, it converts it to the corresponding compass direction.

Clearly a reading of 0 degrees should report back **North**. But what about 1 degrees, 2 degrees, and so on? You need to define a range within which each direction is reported by your program. In the diagram below, the range of degrees for North would be between the red lines.

![Wind vane with some angles](images/wind_vane_shaded.png)

The sample code for the shaded area is done in the WindDirection Class:

```java
    if (degrees >= 338 || degrees < 23) {
            compassDirection = Compass.NORTH;
```

Modify your code to print the compass direction value and test it and put the wind vane in different positions to check whether it works. North on the wind vane can be found by positioning the pointed end of the vane level with the black anemometer port underneath the vane.

The [final code](BlueJFindWindDirection.jar) is here if you want to check your answer.

## Summary

- You have gathered data converted from an analogue voltage to a digital number representation using an ADC, and then converted again to a measurement in degrees.

- Will it matter which way around our weather vane is situated when we put it up outside?

## What next

It would be good if we could represent the wind's direction graphically. you can use the JavaFX class called DirectionGraph to draw a graphic representation of the direction of the wind? 
The example below uses the DirectionGraph to draw compass points before drawing a red line pointing in the direction the wind is blowing from.

![wind direction](images/Winddirection.png)

The solution for that is [here](BlueJWorksheet4.jar)

Can you go one step further and draw a weather map-style arrow, pointing in the direction the wind is blowing TO? To do this, you will need to calculate the angle which is exactly 180 degrees further around the circle than the wind direction. However, simply adding 180 to the measurement won't work; what if the measurement was 350 degrees, for example, since adding 180 to that would result in 530 degrees? To do this correctly, you can use the `MOD` (`%`) operator, which gives the remainder of a division:

```
direction = 350
direction = (direction + 180) % 180
```

`%` performs [integer division](http://mathworld.wolfram.com/IntegerDivision.html) and reports the remainder. So in this case:

```
350 + 180 = 530
530 \ 180 = 2 		# How many times 180 fully goes into 530
530 % 180 = 170		# The remainder of the integer division
```
