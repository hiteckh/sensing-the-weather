# Sensing the Weather - Ambient Temperature Worksheet

In this lesson you will:

- Write code to read information from the ambient temperature sensor
- Combine two programs together
- Produce a graph to display information from two sensor sources at once

## How does the ambient temperature sensor work?

![Ambient Temperature Sensor](images/air_board.png)

This sensor (circled in red on the photograph) can detect the ambient temperature of the air surrounding it.

Temperature sensors can detect changes in temperature and humidity because they are made of materials whose properties change depending on the temperature and the amount of water vapour present in the air.

## Reading from the ambient temperature sensor
Start the BlueJ and create a new project called "ambient_temperature", within the project, create a new Java class called "AmbientTemperature"
You are already familiar with using code written by other people to access information from sensors, as we have used this to gather data from both the wind vane and the soil temperature sensor. 
Here's the packages we need to import for the class:

```java
    import org.bluej.WeatherStation.Units.Temperature;
    import org.bluej.WeatherStation.Implementations.Factory;
    import org.bluej.WeatherStation.Implementations.Platform;
    import org.bluej.WeatherStation.Sensors.TemperatureSensor;
    import org.bluej.WeatherStation.Implementations.PiAmbientTemperatureSensor;
```
We will also need access to the Ambient Temperature sensor, so we should store one as a field and create it in the constructor using the Factory class,
The Factory class is a class for building the sensor set for a given target:
```java
    public AmbientTemperature()
    {
        sensor = new Factory(Platform.WEATHERSTATION_V1).getAmbientTemperatureSensor();
            
    }
```
Note: If you are doing this exercise away from a working weather station,
you can change Platform.WEATHERSTATION_V1 to Platform.MOCK to receive fake data from the sensor instead.

```java
    public Temperature[] readTemperature(int y)
    {
        Temperature[] temperatures = new Temperature[ITERATIONS];
        try{
        for (int i = 0; i < ITERATIONS; i++) 
            {
                Thread.sleep(INTERVAL_MILLIS);
                temperatures[i] = sensor.getTemperature();
                System.out.println(temperatures[i].inCelsius());
            }
            }
        catch (Exception e)
        {
            System.out.println("Can not read from the device");
        }  
        return temperatures; 
    }
 ```       


### Questions

Inspect this code with a partner and answer the following:

- What is `sensor`?
- Which method are we calling on the object to get the temperature reading?
- What units is the temperature given in?

### Answers

- `sensor` is the name of an object we have created to be able to talk to the sensor.
- We call the `getTemperature()` method on the object to get the temperature reading.
- The temperature is given in degrees C.
- 
You can check your code against a completed version [here](BlueJAmbientTemperature.jar).
## Combining the programs

Now that you know how to take a reading from both the ambient temperature sensor and the soil temperature sensor, your challenge is to make a graph with *two* lines, showing the readings from both sensors at once.

1. Save a copy of your soil temperature program as `two_temperature_sensors`

1. Your two lists of values in this code were called `x` and `y`. However, we now want to plot *two* things on the y axis, so change the name of the list `y` to `soil_temp`, then create a new blank list called `amb_temp` where we will store the ambient temperature values.

1. Using the code above for reading from the ambient temperature sensor, alter your program to take a reading from the ambient temperature sensor *as well as* the soil temperature sensor every 5 seconds. Create a class called 'TwoTemperatureSensors', don't forget to set up your sensor before the loop, and take the reading inside the loop.

1. There is 'TwoLineChart' class which is a javaFX class that helps you in creating the chart and plotting the two two line 
1. To plot more than one line on your graph, you need to change your plot code slightly:

	```java
	public void plot()
    {
        Amb=new AmbientTemperature();
        Soil=new SoilTemperature(); 
        amb_temp=Amb.readTemperature();
        soil_temp=Soil.readTemperature();
        TwoLineChart graph=new TwoLineChart();
        graph.x=x;
        graph.soil_temp=soil_temp;
        graph.amb_temp=amb_temp;
        graph.ShowtheGraph();

    }
	
	```
1. Run your code and test whether it works. For testing purposes, you can change the temperature readings by holding the soil temperature sensor in your hand to warm it up, or by blowing gently into one of the holes in the air quality sensor board plastic housing.

1. Now run the experiment; run your program but don't alter the temperature of the sensors. Do they record the same temperature?

The [finished code](BlueJWorksheet6.jar) is here so that you can check your code.

## Summary

- You have combined two different programs into one, demonstrating your understanding of how the code works.
- Are the readings from the different sensors the same? If not, discuss possible reasons why there might be differences in the readings from the different sensors.

## What next

- Can you apply your skills to independently create a graph of data gathered from the anemometer?
