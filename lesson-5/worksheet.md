# Sensing the Weather - Soil Temperature Worksheet

Today you will be learning about the soil temperature sensor.

![Soil temperature sensor](images/soil_temp_sensor.png)

In this lesson you will:

- Connect up the sensor and take a reading
- Store multiple readings from the sensor
- Use Java to plot a graph of your temperature sensor readings

## How does the soil temperature sensor work?

Your soil temperature probe is a prewired and waterproof digital temperature sensor. If you were to take the sensor out of the protective wiring and waterproofing, it would look like this:

![Digital temperature sensor](images/bare_sensor.jpg)

-- Image by oomlout [CC BY-SA 2.0](http://creativecommons.org/licenses/by-sa/2.0), via Wikimedia Commons --

The sensor has three pins: a ground pin, a data pin, and a 3.3V power pin. You may have noticed that the sensor was described as a *digital* temperature sensor; this is because the signals generated are converted into a digital format that the Raspberry Pi can understand.

To initiate a temperature measurement, the Raspberry Pi sends a command to the sensor. Then the resulting thermal data is stored in a temperature register in the sensor memory for the Raspberry Pi to read.


## Taking a reading

1. Set up and boot your Raspberry Pi Weather Station, including the smaller box which contains the air quality sensor board and to which the soil temperature probe is connected.
1. The first thing we need to know is how to take a simple reading from the temperature sensor. The temperature sensor has some prewritten code we can use; it has a bit of a confusing name because the actual name of the temperature sensor component is **DS18B20**. 
1. Start the BlueJ and create a new project called "Soil_Temperature", within the project, create a new Java class called "SoilTemperature" 
1. We need to import the DS18B20 into the class as:

```java
	import org.bluej.WeatherStation.Drivers.DS18B20
```
1. In the constructor method, we create an object `soil_probe_temp` we can use to talk to the sensor, and then calls the method `read()` on this object to get the current temperature reading.
but you need to include try catch block as this class thows an exaeoptipn

```java
	try{
            DS18B20 soil_probe_temp=new DS18B20();
        }
        catch (Exception e){
            System.out.println("Driver DS18B20 not found");

        }
```

1. Then you need to call method to read the temp, this should be inclided inside a method called readTemperature():
```java
public double readTemperature(int y)
    {
            double temp=0;
            try
            {
            temp=soil_probe_temp.read();
            }

            catch (Exception e)
            {
            System.out.println("Driver DS18B20 not found");
            }
        return temp;     
    }
```
1. Now think back to the wind gust speed lesson where we took multiple readings from the anemometer and stored the speeds in an array. Can you work out how to take 10 readings and store them in an array? You might want to use your [old code](../lesson-3/) to help you.

1. It's not much use to us if we take temperature readings immediately one after the other. Can you work out how to space the readings at an interval of 5 seconds?

1. Here is the [finished code](BlueJSoilTemperatureReadings.jar) if you get stuck or want to check your answer.

## Plotting a graph

### Question

Which type of graph would best show the data we have gathered from the temperature sensor?

### Answer

A line graph would be the most suitable graph to show this data, as it shows the change in temperature over time.

A JavaFX class in the BlueJ project is provided in this exercise "LineChartSample" to help you write code to create graphs of our data. 

1. You have already written code to generate an aarray of 10 temperature readings and store them in an array. Rename the array to `y` as it contains the values that will be plotted on the `y` axis of your graph.

1. To be able to build a graph, we also need some values for the `x` axis of our graph. These values should represent the points in time when we took a sample. In your code, you took a temperature reading every 5 seconds, so our `x` axis values should look like this:

	```java
	int[] x={0, 5, 10, 15, 20, 25, 30, 35, 40, 45};
	int[] y=new int[10];
	```
Using this as an example, can you write some code to generate an array which looks like the `x` axis values we want (from 0 seconds to 45 seconds, with a value every 5 seconds)?

1. Create a method called plot() in the SoilTmperature class, Combine your code togther and it should look a bit like this, the LineChartSample is a javaFX class created to help in this exercise, create new instance caled graph and use the x,y variables to set the values fo the 'x' and 'y' axex

	```java
        public void plot()
        {
            int[] x={0, 5, 10, 15, 20, 25, 30, 35, 40, 45};
            double[] y=new double[10];
            y=readTemperature();
            LineChartSample graph=new LineChartSample();
            graph.x=x;
            graph.y=y;
            graph.ShowtheGraph();
        }
	```

1. Now create an object of the class SoilTemperature in the BlueJ project and run the method plot() on it.

1. Test your code; you can cause temperature variations by simply holding the sensor in your hand to warm it up. 

1. Your graph doesn't have any axis labels or a title. Can you work out how to add these?

	![Example graph](images/temperatures.png)

The answer is [here](BlueJWorksheet5.jar) if you get stuck.

## Summary

- In this lesson you recorded multiple readings from the temperature sensor, spaced at even time intervals
- You have used JavaFX to automatically generate a graph of your results


## What next

- Can you alter the program to make it possible to customise the number of readings taken by the sensor? At the moment the sensor will always take 10 readings, but could the user type in how many readings they want?
- Could the user specify the interval they would like to sample at?
- Can you work out how to save your graph as a PDF using functions from the JavaFX?
