# Sensing the Weather - Barometric Pressure Worksheet
In this lesson you will:

- Be able to take a reading from the barometric pressure sensor
- Understand what pressure is and the units it's measured in
- Be able to store data in CSV format and access it from other applications

## How does the barometric pressure sensor work?

Here is the barometric pressure sensor supplied with the Raspberry Pi Weather Station kit as part of the air quality sensor board:

![Barometric pressure sensor](images/pressure_sensor.png)

Our barometric pressure sensor measures pressure in pascals (Pa). One pascal is equal to one newton per square metre.

1Pa = 1N/m<sup>2</sup>


## Taking a reading

1. Start the BlueJ and create a new project called 'BarometricPressure', within the project, create a new Java class called 'Barometric_Pressure'
1. Import the follwoing packages for the class:

```java
	import org.bluej.WeatherStation.Units.Pressure;
    import org.bluej.WeatherStation.Implementations.Factory;
    import org.bluej.WeatherStation.Implementations.Platform;
    import org.bluej.WeatherStation.Sensors.PressureSensor;
    import org.bluej.WeatherStation.Drivers.AirPressureTemperature;
    import org.bluej.WeatherStation.Implementations.PiPressureSensor;
    import javafx.application.Application;
```
We will also need access to the Barometric Pressure sensor, so we should store one as a field and create it in the constructor using the Factory class,
The Factory class is a class for building the sensor set for a given target:
```java
    public BarometricPressure()
    {
      sensor = new Factory(Platform.WEATHERSTATION_V1).getPressureSensor();
    }
```
Note: If you are doing this exercise away from a working weather station,
you can change Platform.WEATHERSTATION_V1 to Platform.MOCK to receive fake data from the sensor instead.

Create a new method called readAirQuality() to read the pressure of the Barometric Pressure sensor

```java
    public double readAirQuality()
    {
        Pressure pressure;
        pressure = sensor.getPressure();
        System.out.println(pressure.inAtmospheres());
        return pressure.inAtmospheres();
    }
```
## Changes in barometric pressure over time

### Question

Will the pressure value measured by the barometric pressure sensor change over time?

### Answer

Yes, but do you know *why*?

We're going to take a series of readings from the barometric pressure sensor at intervals over time, and record these readings, along with the time and date they were taken, in a format called a CSV file. A CSV file is simply a text file where data are stored, with commas in between them to separate them out. An example of data stored in CSV file might look like this:

```
25/06/16, 14:30, 10345
```

1. Modify your code to take a series of readings from the barometric sensor.

1. We'll need to calculate the time and date that a reading was taken, so that we can store this in the CSV file along with each reading. 
2. To do this we will use the Java libraries called `Date` and  `SimpleDateFormat`, Add this line of code next to your other `import` statement to import this library into your program:
```java
     import java.util.Date;
     import java.text.SimpleDateFormat;
```
1. The Date class is really usefull because it generates a time and/or date in whatever format you specify. Add some code to generate the current date in a format you would like. If you wish, you can change the format (e.g. if you would like to use the US date format). You can use the [java documentation](https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html) for `strftime` to find out which other options are available.

1. Now add some more code to generate the current time in a format of your choice, and save it in a new variable called `current_time`.

1. Now add a final variable called `current_pressure` and generate a pressure reading from the sensor. Use the code from the previous section to help you with this.


## Saving data to a CSV file

1. We need to save these values to a CSV file, which is basically just a text file where we write the data in a special format. Import the follwing packages to open a file in Java:

```java
    import java.io.FileWriter;
    import java.io.FileNotFoundException;
    import java.io.PrintWriter;
```
1. To open a file, add this line into your code:

```java
FileWriter csvFile =new FileWriter("pressure.csv",true);
```
1. Now we will write our data to the file. When we opened the file we created a **PrintWriter** called `write` which points to where our file is stored in memory. We use this file pointer to access the file and write data into it, like this:

```java
PrintWriter writer = new PrintWriter(csvFile);
```
1. Finally, it's important to close the file at the end of the program, so add the following line of code:

```java
	writer.close();
```
1. Run your program and check that a file called `pressure.csv` is created in the same folder as your code, and that it contains the word "Data". If it has worked, delete the word "Data" from the file and save it as a blank file, as we don't want this test data as one of the barometric pressure readings!

1. To write data in CSV format, we simply add commas between the values. Here we have started creating a string to write to the file, to show you how to add the commas:

```java
	       StringBuilder sb = new StringBuilder();
           sb.append(pressure.inAtmospheres());
           sb.append(',');
           writer.write(sb.toString());
```
Find the place in your program where you open the file and after that line, type in the line of code above. Finish off the line of code by adding in the `current_time` and `current_pressure` data, converted to a string and separated by a comma. At the end of the line, add the newline character `"\n"` to tell the file to start the next piece of data on a new line.

## Automating the readings

You already know how to automate things happening at an interval. This code is from the lesson on air quality.

Can you alter your code to make your program write a line to the CSV file every 30 minutes? Don't forget that you'll need to take new readings *within* the loop, otherwise the same values will be written over and over again to your CSV file!

The finished code is [here](BlueJWorksheet9.jar) if you would like to compare it.

## Summary

- You have taken readings over a time period from the barometric pressure sensor and stored them as a CSV file.
- How could you use this data in another program?
- Why do you think the readings from the pressure sensor change over time?

## What next

- Using the data you have gathered from the barometric pressure sensor, import the CSV file into another application and use that application to analyse the data. Perhaps you could import into Excel or Google Sheets and draw a graph of the pressure readings over time?
